package nl.saxion.smartreporting.model

import nl.saxion.smartreporting.ReportManager
import java.time.LocalDate
import java.util.*

class Client(val id: Int, val firstName: String, val lastName: String, val birthDate : LocalDate, val gender: Gender,
             val height: Double, val address: String, val postalCode: String, val city: String, val phone: String
             , val phone2: String, val email: String, val email2: String, val profileImage: String) {


    private var reports: MutableList<Report> = mutableListOf()


    fun addReport(report: Report) {
        reports.add(report)

    }

    fun getReports():List<Report>{
        return reports.toList()
    }

    fun getPreviousReport(): Report{
        return reports.last()
    }

    fun getReportById(id: Int): Report{
        return reports.filter { it.id == id }.first()
    }

    fun getReportsByClient(): List<Report>?{
        val client = ReportManager.clients.find { it.id == this.id }
        if(client != null){
            return client.getReports()
        }
        return null
    }



}

