package nl.saxion.smartreporting.model

data class Caregiver(val id: Int, val firstname:String, val lastname:String, val password: String)