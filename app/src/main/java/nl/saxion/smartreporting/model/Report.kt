package nl.saxion.smartreporting.model

import java.sql.Timestamp

data class Report(
    val timestamp: Timestamp,
    val id: Int,
    val person_id: Int,
    val caregiver: Caregiver,
    val weight: Int,
    val bloodPressure: Int,
    val bodyTemperature: Double,
    val description: String
)




