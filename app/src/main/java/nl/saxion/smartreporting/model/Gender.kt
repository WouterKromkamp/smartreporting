package nl.saxion.smartreporting.model;

enum class Gender {
    Male, Female, Unspecified, ApacheAttackHelicopter;

}