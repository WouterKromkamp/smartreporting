package nl.saxion.smartreporting

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import nl.saxion.smartreporting.model.Client
import nl.saxion.smartreporting.view.ClientDiffUtilCallback
import nl.saxion.smartreporting.view.ClientRecyclerAdapter
import nl.saxion.smartreporting.view.ClientViewModel
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: ClientViewModel
    private val disposable = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ReportManager.generateFakedata()

        viewModel = ViewModelProviders.of(this).get(ClientViewModel::class.java)
        viewModel.oldFilteredClients.clear()

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ClientRecyclerAdapter(this, viewModel.oldFilteredClients, { client : Client -> clientClicked(client) })

        searchInput
            .textChanges()
            .debounce(200, TimeUnit.MILLISECONDS)
            .subscribe {
                viewModel
                    .search(it.toString())
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        val diffResult = DiffUtil.calculateDiff(ClientDiffUtilCallback(viewModel.oldFilteredClients, viewModel.filteredClients))
                        viewModel.oldFilteredClients.clear()
                        viewModel.oldFilteredClients.addAll(viewModel.filteredClients)
                        diffResult.dispatchUpdatesTo(recyclerView.adapter as ClientRecyclerAdapter)
                    }.addTo(disposable)
            }.addTo(disposable)
    }

    private fun clientClicked(client : Client) {
        val intent = ClientActivity.newIntent(this, client)
        startActivity(intent)
//        Toast.makeText(this, "Clicked: ${client.firstName}", Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    fun clickme(){

    }
}
