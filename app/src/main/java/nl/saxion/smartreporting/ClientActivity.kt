package nl.saxion.smartreporting

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_client.*
import nl.saxion.smartreporting.model.Client
import nl.saxion.smartreporting.model.Report
import nl.saxion.smartreporting.view.SectionsPagerAdapter
import nl.saxion.smartreporting.view.fragment.OverviewFragment
import nl.saxion.smartreporting.view.fragment.ReportFragment

class ClientActivity : AppCompatActivity(), OverviewFragment.OnMeasurementListInteractionListener, ReportFragment.OnReportListInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_client)

        val clientId = intent.getIntExtra(INTENT_CLIENT_ID, -1)

//        Toast.makeText(this, "Clicked client ID: ${clientId}", Toast.LENGTH_LONG).show()

        setSupportActionBar(toolbar)
        supportActionBar?.setTitle("Client detail")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val sectionsPagerAdapter =
            SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
    }

    override fun onMeasurementInteraction(report: Report?) {
        if (report != null) {
//            Toast.makeText(this, "Clicked report #${report.id}", Toast.LENGTH_LONG).show()
        }
    }

    override fun onReportInteraction(report: Report) {
        val intent = ReportActivity.newIntent(this, report.id,report.person_id)
        intent.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        startActivity(intent)

    }


    companion object {

        private val INTENT_CLIENT_ID = "client_id"

        fun newIntent(context: Context, client: Client): Intent {
            val intent = Intent(context, ClientActivity::class.java)
            intent.putExtra(INTENT_CLIENT_ID, client.id)
            return intent
        }
    }

}