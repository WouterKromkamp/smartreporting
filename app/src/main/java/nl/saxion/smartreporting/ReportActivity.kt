package nl.saxion.smartreporting

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_report.*
import nl.saxion.smartreporting.model.Client
import nl.saxion.smartreporting.model.Report

class ReportActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)

        val report_id = intent.getIntExtra("report_id",-1)
        val client_id = intent.getIntExtra("client_id",-1)
        val client: Client? = ReportManager.clients.find { it.id == client_id }

        val report = client!!.getReportById(report_id)

        reportId.text = report.id.toString()
        reportCaregiverId.text = report.caregiver.id.toString()
        reportTime.text = report.timestamp.toString()
        reportClientName.text = report.person_id.toString()
        reportWeight.text = report.weight.toString()
        reportBloodPressure.text = report.bloodPressure.toString()
        reportTemperature.text = report.bodyTemperature.toString()
        reportDescription.text = report.description

        
    }

    companion object {

        private val INTENT_REPORT_ID = "report_id"
        private val INTENT_CLIENT_ID = "client_id"

        fun newIntent(context: Context, report: Int, client: Int): Intent {
            val intent = Intent(context, ReportActivity::class.java)
            intent.putExtra(INTENT_CLIENT_ID, client)
            intent.putExtra(INTENT_REPORT_ID, report)
            return intent
        }

    }

}
