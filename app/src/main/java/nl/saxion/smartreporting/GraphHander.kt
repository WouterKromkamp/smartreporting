package nl.saxion.smartreporting

import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import nl.saxion.smartreporting.model.Client

class GraphHander {


    fun getReportsDates(client: Client): Array<String> {


        var dates = mutableListOf<String>()

        for (report in client.getReports()) {


            dates.add(report.timestamp.toString())
        }

        return dates.toTypedArray();
    }

    fun getWeightData(client: Client): LineData {


        var entries = mutableListOf<Entry>();

        var i = 0;
        for (report in client.getReports()) {


            entries.add(Entry(i.toFloat(), report.weight.toFloat()));


            i++;


        }

        var lineDataSet = LineDataSet(entries, "weight")
        var dataset = LineData(lineDataSet);
        lineDataSet.setDrawValues(false);
        lineDataSet.fillAlpha = 100;




        return dataset

    }
}

