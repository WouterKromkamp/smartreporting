package nl.saxion.smartreporting

import nl.saxion.smartreporting.model.Caregiver
import nl.saxion.smartreporting.model.Client
import nl.saxion.smartreporting.model.Gender
import nl.saxion.smartreporting.model.Report
import java.sql.Timestamp
import java.time.LocalDate

import java.util.*


class ReportManager {


    companion object {

        @JvmStatic
        var caregivers:MutableList<Caregiver> = mutableListOf()
        var clients:MutableList<Client> = mutableListOf()
        private var alreadyGenerated = false


        fun generateFakedata(){
            if (!alreadyGenerated) {
                caregivers.add(Caregiver(1, "Annie", "Jansen", "Nedap123"))
                clients.add(
                    Client(
                        1,
                        "Wessel",
                        "Perik",
                        LocalDate.parse("2018-12-12"),
                        Gender.Male,
                        1.95,
                        "bornerbroekerstraat 20",
                        "3216 AB",
                        "Bornerbroek",
                        "1234567891",
                        "612345678",
                        "wesselperik@gmail.com",
                        "",
                        "./images/wessel.png"
                    )
                )
                clients.first().addReport(
                    Report(
                        Timestamp(System.currentTimeMillis()),
                        1,
                        1,
                        caregivers.first(),
                        84,
                        120,
                        37.0,
                        "Is alcholist"
                    )
                )
                clients.first().addReport(
                    Report(
                        Timestamp(System.currentTimeMillis()),
                        2,
                        1,
                        caregivers.first(),
                        83,
                        115,
                        37.5,
                        "Is still alcholist"
                    )
                )

                clients.first().addReport(
                    Report(
                        Timestamp(System.currentTimeMillis()-3),
                        3,
                        1,
                        caregivers.first(),
                        83,
                        120,
                        37.0,
                        "Is alcholist"
                    )
                )

                clients.first().addReport(
                    Report(
                        Timestamp(System.currentTimeMillis()),
                        4,
                        1,
                        caregivers.first(),
                        90,
                        120,
                        37.0,
                        "Is alcholist"
                    )
                )
                alreadyGenerated = true
            }

        }
    }



}