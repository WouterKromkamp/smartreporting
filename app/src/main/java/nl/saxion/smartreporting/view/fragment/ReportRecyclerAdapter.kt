package nl.saxion.smartreporting.view.fragment

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import nl.saxion.smartreporting.R

import kotlinx.android.synthetic.main.item_report.view.*
import nl.saxion.smartreporting.model.Client
import nl.saxion.smartreporting.model.Report
import java.text.SimpleDateFormat

class ReportRecyclerAdapter(
    private val context: Context, private val client: Client, private val reports: List<Report>, val clickListener: (Report) -> Unit
) : RecyclerView.Adapter<ReportRecyclerAdapter.ReportHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportHolder {
        return ReportHolder(LayoutInflater.from(context).inflate(R.layout.item_report, parent, false), context)
    }

    override fun onBindViewHolder(holder: ReportRecyclerAdapter.ReportHolder, position: Int) {
        holder.bind(reports[position], clickListener)
    }

    override fun getItemCount(): Int = reports.size

    class ReportHolder(itemView: View, private val context: Context) : RecyclerView.ViewHolder(itemView) {
        fun bind(report: Report, clickListener: (Report) -> Unit) {
            val sdf = SimpleDateFormat("MM/dd/yyyy HH:mm")
            itemView.titleView.text = "Report #${report.id} - ${sdf.format(report.timestamp)}"
            itemView.descriptionView.text = "Reported by ${report.caregiver.firstname} ${report.caregiver.lastname}"
            itemView.setOnClickListener{ clickListener(report)}

        }

    }
}
