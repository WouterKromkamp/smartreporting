package nl.saxion.smartreporting.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.mikephil.charting.charts.LineChart
import nl.saxion.smartreporting.GraphHander
import nl.saxion.smartreporting.R
import nl.saxion.smartreporting.ReportManager
import nl.saxion.smartreporting.model.Report

class OverviewFragment : Fragment() {

    private var listener: OnMeasurementListInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_overview, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
//                adapter = ReportRecyclerAdapter(context, ReportManager.clients.first(), ReportManager.clients.first().getReports(), listener)
            }
        }

        ReportManager.generateFakedata();
        var linechart = view.findViewById<View>(R.id.chart) as LineChart
        linechart.setDrawGridBackground(false);
        var graphHandler = GraphHander();
        linechart.getAxisLeft().setDrawGridLines(false);
        linechart.axisRight.setDrawGridLines(false);
        linechart.getXAxis().setDrawGridLines(false);
        linechart.description.text = "Wessel's Weight";
        linechart.axisRight.setDrawLabels(false);
        linechart.xAxis.setDrawLabels(false);


        linechart.data = graphHandler.getWeightData(ReportManager.clients.get(0));

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnMeasurementListInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnMeasurementListInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnMeasurementListInteractionListener {
        fun onMeasurementInteraction(report: Report?)
    }
}
