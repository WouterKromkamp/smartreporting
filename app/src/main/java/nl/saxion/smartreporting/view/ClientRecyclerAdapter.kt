package nl.saxion.smartreporting.view

import android.content.Context
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.item_client.view.*
import nl.saxion.smartreporting.ClientActivity
import nl.saxion.smartreporting.R
import nl.saxion.smartreporting.ReportActivity
import nl.saxion.smartreporting.model.Client
import java.text.SimpleDateFormat
import java.time.LocalTime

class ClientRecyclerAdapter(private val context: Context, private val clients: List<Client>, val clickListener: (Client) -> Unit) : RecyclerView.Adapter<ClientRecyclerAdapter.ClientHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientHolder {
        return ClientHolder(LayoutInflater.from(context).inflate(R.layout.item_client, parent, false))
    }

    override fun getItemCount() = clients.size

    override fun onBindViewHolder(holder: ClientHolder, position: Int) {
        holder.bind(clients[position], clickListener)
    }


    class ClientHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(client: Client, clickListener: (Client) -> Unit) {
            val sdf = SimpleDateFormat("MM/dd/yyyy HH:mm")
            itemView.nameView.text = "${client.firstName} ${client.lastName}"
            itemView.descriptionView.text = "Last report: ${sdf.format(client.getPreviousReport().timestamp)}"
            itemView.setOnClickListener { clickListener(client) }
        }




    }


}