package nl.saxion.smartreporting.view

import android.arch.lifecycle.ViewModel
import io.reactivex.Completable
import nl.saxion.smartreporting.ReportManager
import nl.saxion.smartreporting.model.Client

class ClientViewModel : ViewModel() {

    val originalClients = ReportManager.clients;

    val filteredClients: MutableList<Client> = mutableListOf()
    val oldFilteredClients: MutableList<Client> = mutableListOf()

    init {
        oldFilteredClients.clear()
        oldFilteredClients.addAll(originalClients)
    }

    fun search(query: String): Completable = Completable.create {
        val wanted = originalClients.filter {
            it.firstName.toLowerCase().contains(query.toLowerCase()) || it.lastName.toLowerCase().contains(query.toLowerCase())
        }.toList()

        filteredClients.clear()
        filteredClients.addAll(wanted)
        it.onComplete()
    }
}