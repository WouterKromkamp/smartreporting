package nl.saxion.smartreporting.view

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import nl.saxion.smartreporting.view.fragment.OverviewFragment
import nl.saxion.smartreporting.view.fragment.ReportFragment

class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        if (position == 0) return OverviewFragment()
        else return ReportFragment()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "Overview"
            1 -> return "Reports"
            else -> {
                return "Unknown"
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }
}