package nl.saxion.smartreporting.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import nl.saxion.smartreporting.R
import nl.saxion.smartreporting.ReportManager
import nl.saxion.smartreporting.model.Client
import nl.saxion.smartreporting.model.Report

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ReportFragment.OnListFragmentInteractionListener] interface.
 */
class ReportFragment : Fragment(){

    private var listener: OnReportListInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_report, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
                adapter = ReportRecyclerAdapter(context, ReportManager.clients.first(), ReportManager.clients.first().getReports(), { report : Report -> listener?.onReportInteraction(report) } )
            }
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnReportListInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnReportListInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnReportListInteractionListener {
        fun onReportInteraction(report: Report)
    }
}
