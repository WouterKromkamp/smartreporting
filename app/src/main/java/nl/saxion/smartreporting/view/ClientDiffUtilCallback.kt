package nl.saxion.smartreporting.view

import android.support.v7.util.DiffUtil
import nl.saxion.smartreporting.model.Client

class ClientDiffUtilCallback(private val oldList: List<Client>, private val newList: List<Client>) : DiffUtil.Callback() {
    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = oldList[oldItemPosition].id == newList[newItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = true
}